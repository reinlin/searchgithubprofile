package com.rein.searchgithubprofile

import android.app.Activity
import android.app.Application
import com.rein.searchgithubprofile.di.AppComponent
import com.rein.searchgithubprofile.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.HasActivityInjector

class SearchApplication : Application(), HasActivityInjector {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    override fun activityInjector(): AndroidInjector<Activity> =
        appComponent.activityInjector
}