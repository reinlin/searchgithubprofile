package com.rein.searchgithubprofile.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.rein.lib.model.Profile
import com.rein.lib.util.ioThread

@Database(
    entities = [ProfileEntity::class],
    version = 1,
    exportSchema = false
)
abstract class ProfilesDatabase: RoomDatabase() {

    abstract fun profileDao(): ProfileDao

    companion object {
        fun getInstance(context: Context): ProfilesDatabase =
            Room.databaseBuilder(context, ProfilesDatabase::class.java, "profilesDb")
                .build()
    }
}
