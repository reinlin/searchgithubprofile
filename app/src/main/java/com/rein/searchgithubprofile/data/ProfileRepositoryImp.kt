package com.rein.searchgithubprofile.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.rein.lib.api.GithubApi
import com.rein.lib.domain.BoundaryCallback
import com.rein.lib.domain.ProfileRepository
import com.rein.lib.model.*
import com.rein.searchgithubprofile.data.db.ProfilesDatabase
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executors

class ProfileRepositoryImp constructor(
    private val profileDB: ProfilesDatabase,
    private val githubApi: GithubApi
) : ProfileRepository {

    private val diskIO by lazy { Executors.newFixedThreadPool(5) }
    lateinit var pagedList: LiveData<PagedList<Profile>>

    override fun search(keyword: String, pageSize: Int, error: (message: String) -> Unit): NetworkResult {

        val refreshTrigger = MutableLiveData<Unit>()
        val networkState = Transformations.switchMap(refreshTrigger) {
            connectSearch(keyword)
        }
        val boundaryCallback = BoundaryCallback(keyword) { input ->
            getCount(input)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    { count ->
                        if (count == 0) refreshTrigger.value = null
                    },
                    { throwable ->
                        throwable.message?.let(error)
                    }
                )
        }

        val profiles = getUsers(keyword)
        pagedList = LivePagedListBuilder(profiles, pageSize)
            .setBoundaryCallback(boundaryCallback)
            .build()

        return NetworkResult(
            profileList = pagedList,
            networkState = networkState,
            refresh = {
                refreshTrigger.value = null
            }
        )
    }

    private fun connectSearch(keyword: String): LiveData<NetworkState> {
        val networkState = MutableLiveData<NetworkState>()
        networkState.value = NetworkState.LOADING
        githubApi.search("$keyword+in:login").enqueue(object : Callback<GitUser> {

            override fun onFailure(call: Call<GitUser>, t: Throwable) {
                networkState.value = NetworkState.error(t.message)
            }

            override fun onResponse(call: Call<GitUser>, response: Response<GitUser>) {
                diskIO.execute {
                    profileDB.runInTransaction {
                        if (response.code() == 200) {
                            response.body()?.items
                                ?.map { DbProfileMapper.toDb(Profile(it.id, it.login, it.avatarUrl)) }
                                ?.let { profileEntities ->
                                    profileDB.profileDao().deleteProfiles()
                                    profileDB.profileDao().insertAll(profileEntities)
                                }
                            networkState.postValue(NetworkState.SUCCESS)
                        } else
                            networkState.postValue(NetworkState.error(response.message()))
                    }

                }
            }

        })

        return networkState
    }


    override fun insertAll(profiles: List<Profile>): Completable =
        Completable.fromAction {
            profileDB.profileDao().insertAll(profiles.map {
                DbProfileMapper.toDb(it)
            })
        }

    override fun allProfiles(): DataSource.Factory<Int, Profile> =
        profileDB.profileDao().allProfiles().map {
            DbProfileMapper.fromDb(it)
        }

    override fun getUsers(keyword: String): DataSource.Factory<Int, Profile> =
        profileDB.profileDao().getProfiles("%$keyword%").map {
            DbProfileMapper.fromDb(it)
        }

    override fun getCount(keyword: String): Single<Int> =
        profileDB.profileDao().getCount("%$keyword%")

    override fun unregister() {
        diskIO.shutdown()
    }
}

