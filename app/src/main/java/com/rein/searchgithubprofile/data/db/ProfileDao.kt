package com.rein.searchgithubprofile.data.db

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface ProfileDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(profiles: List<ProfileEntity>)

    @Query("SELECT * FROM profiles ORDER BY id DESC")
    fun allProfiles(): DataSource.Factory<Int, ProfileEntity>

    @Query("DELETE FROM profiles")
    fun deleteProfiles()

    @Query("SELECT * FROM profiles WHERE name LIKE :keyword")
    fun getProfiles(keyword: String): DataSource.Factory<Int, ProfileEntity>

    @Query("SELECT COUNT(*) FROM profiles WHERE name LIKE :keyword")
    fun getCount(keyword: String): Single<Int>
}