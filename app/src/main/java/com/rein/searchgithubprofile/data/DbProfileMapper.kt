package com.rein.searchgithubprofile.data

import com.rein.lib.model.Profile
import com.rein.searchgithubprofile.data.db.ProfileEntity

object DbProfileMapper {

    fun fromDb(from: ProfileEntity) =
        Profile(
            id = from.id.toInt(),
            login = from.name,
            avatarUrl = from.avatarUrl
        )

    fun toDb(from: Profile) =
        ProfileEntity(
            id = from.id.toLong(),
            name = from.login,
            avatarUrl = from.avatarUrl ?: ""
        )
}