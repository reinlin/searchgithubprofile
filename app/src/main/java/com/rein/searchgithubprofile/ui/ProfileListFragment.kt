package com.rein.searchgithubprofile.ui

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.rein.lib.model.NetworkState
import com.rein.lib.model.Status
import com.rein.lib.ui.GetProfilesViewModelFactory
import com.rein.lib.ui.list.ProfileListAdapter
import com.rein.lib.ui.list.ProfileListViewModel
import com.rein.searchgithubprofile.R
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_profile_list.*
import javax.inject.Inject

class ProfileListFragment: Fragment() {

    @Inject
    lateinit var getProfilesViewModelFactory: GetProfilesViewModelFactory

    private val profileAdapter = ProfileListAdapter()
    private lateinit var viewModel: ProfileListViewModel

    companion object {
        fun getInstance(): ProfileListFragment =
            ProfileListFragment()
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileListView.apply {
            layoutManager = LinearLayoutManager(this@ProfileListFragment.context)
            adapter = profileAdapter
        }

        viewModel = ViewModelProviders.of(this, getProfilesViewModelFactory).get(ProfileListViewModel::class.java).apply {
            profileList.observe(this@ProfileListFragment, Observer { pagedList ->
                profileAdapter.submitList(pagedList)
                isLoading(false)
            })
            networkState.observe(this@ProfileListFragment, Observer {
                showMessage(it)
            })
            message.observe(this@ProfileListFragment, Observer {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            })
        }

        swipeRefresh.setOnRefreshListener {
            profileAdapter.submitList(null)
            viewModel.refresh()
        }

        inputSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                inputKeyword()
                true
            }
            else
                false
        }

        inputSearch.setOnKeyListener { _, ketCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && ketCode == KeyEvent.KEYCODE_ENTER) {
                inputKeyword()
                true
            }
            else
                false
        }
    }

    private fun inputKeyword() {
        inputSearch.text?.trim().toString().let { keyword ->
            if (keyword.isNotEmpty()) {
                if (viewModel.updateKeyword(keyword)) {
                    profileListView.scrollToPosition(0)
                    profileAdapter.submitList(null)
                }
            }
        }
    }

    private fun isLoading(isLoading: Boolean) {
        inputSearch.isEnabled = isLoading.not()
        swipeRefresh.isRefreshing = isLoading
    }

    private fun showMessage(state: NetworkState) {
        isLoading(state.status == Status.LOADING)
        if (state.status == Status.FAILED) state.message?.let { message ->
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}