package com.rein.searchgithubprofile.di

import com.rein.lib.domain.ProfileRepository
import com.rein.lib.ui.GetProfilesViewModelFactory
import com.rein.searchgithubprofile.ui.MainActivity
import com.rein.searchgithubprofile.ui.ProfileListFragment
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class UIModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeProfileListFragment(): ProfileListFragment

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideGetProfilesViewModelFactory(repository: ProfileRepository) : GetProfilesViewModelFactory =
            GetProfilesViewModelFactory(repository)
    }
}