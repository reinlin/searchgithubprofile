package com.rein.searchgithubprofile.di

import android.app.Application
import android.content.Context
import com.rein.lib.api.GithubApi
import com.rein.lib.domain.ProfileRepository
import com.rein.searchgithubprofile.data.ProfileRepositoryImp
import com.rein.searchgithubprofile.data.db.ProfileDao
import com.rein.searchgithubprofile.data.db.ProfilesDatabase
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
object DataModule {

    @Provides
    @Singleton
    @JvmStatic
    fun provideContext(application: Application): Context =
        application.applicationContext

    @Provides
    @Singleton
    @JvmStatic
    fun provideProfileDatabase(context: Context): ProfilesDatabase =
        ProfilesDatabase.getInstance(context)

    @Provides
    @Singleton
    @JvmStatic
    fun provideRepository(profileDB: ProfilesDatabase, githubApi: GithubApi): ProfileRepository =
        ProfileRepositoryImp(profileDB, githubApi)
}