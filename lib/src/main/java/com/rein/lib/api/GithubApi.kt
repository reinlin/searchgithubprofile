package com.rein.lib.api

import com.rein.lib.model.GitUser
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {
    @GET("search/users")
    fun search(@Query(value = "q", encoded = true) keyword: String): Call<GitUser>
}