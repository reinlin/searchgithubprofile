package com.rein.lib.domain

import androidx.paging.PagedList
import com.rein.lib.model.Profile

class BoundaryCallback(
    private val keyword: String,
    private val query: (keyword: String) -> Unit
) : PagedList.BoundaryCallback<Profile>() {

    override fun onZeroItemsLoaded() {
        query(keyword)
    }
}