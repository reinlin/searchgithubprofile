package com.rein.lib.domain

import androidx.paging.DataSource
import com.rein.lib.model.NetworkResult
import com.rein.lib.model.Profile
import io.reactivex.Completable
import io.reactivex.Single


interface ProfileRepository {

    fun insertAll(profiles: List<Profile>): Completable

    fun allProfiles(): DataSource.Factory<Int, Profile>

    fun search(keyword: String, pageSize: Int, error: (message: String) -> Unit): NetworkResult

    fun getUsers(keyword: String): DataSource.Factory<Int, Profile>

    fun getCount(keyword: String): Single<Int>

    fun unregister()
}