package com.rein.lib.util

import java.util.concurrent.Executors

fun ioThread(f: () -> Unit) {
    Executors.newSingleThreadExecutor().execute(f)
}