package com.rein.lib.model

import com.squareup.moshi.Json

data class Profile(
    @Json(name = "id")
    val id: Int,
    @Json(name = "login")
    val login: String,
    @Json(name = "avatar_url")
    val avatarUrl: String? = null
)