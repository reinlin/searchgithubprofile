package com.rein.lib.model


import com.squareup.moshi.Json

data class GitUser(
    @Json(name = "incomplete_results")
    val incompleteResults: Boolean,
    @Json(name = "items")
    val items: List<Item>,
    @Json(name = "total_count")
    val totalCount: Int
)