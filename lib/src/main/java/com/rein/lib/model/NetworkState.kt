package com.rein.lib.model

enum class Status {
    LOADING,
    SUCCESS,
    FAILED
}

data class NetworkState constructor(
    val status: Status,
    val message: String? = null
){
    companion object {
        val SUCCESS = NetworkState(Status.SUCCESS)
        val LOADING = NetworkState(Status.LOADING)
        fun error(message: String?) = NetworkState(Status.FAILED, message)
    }
}