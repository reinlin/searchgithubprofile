package com.rein.lib.model

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

data class NetworkResult(
    val profileList: LiveData<PagedList<Profile>>,
    val networkState: LiveData<NetworkState>,
    val refresh: () -> Unit
)