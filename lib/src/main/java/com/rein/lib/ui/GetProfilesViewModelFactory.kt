package com.rein.lib.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rein.lib.domain.ProfileRepository
import com.rein.lib.ui.list.ProfileListViewModel

class GetProfilesViewModelFactory (
    private val repository: ProfileRepository
) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProfileListViewModel::class.java))
            return ProfileListViewModel(repository) as T
        throw IllegalAccessException("Unknown ViewModel class")
    }
}