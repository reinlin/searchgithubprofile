package com.rein.lib.ui.list

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rein.lib.R
import com.rein.lib.model.Profile
import java.util.logging.Logger

class ProfileListAdapter: PagedListAdapter<Profile, ProfileListAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_profile, parent, false)) {

        private val nameText: TextView = itemView.findViewById(R.id.profileName)
        private val avatarImage: ImageView = itemView.findViewById(R.id.profileAvatar)

        fun bind(profile: Profile?) {
            profile?.let {
                Log.d("profile", "${it.login}, ${it.avatarUrl}")
            }
            nameText.text = profile?.login
            profile?.avatarUrl?.let { url ->
                Glide.with(itemView.context)
                    .load(url)
                    .centerCrop()
                    .placeholder(android.R.drawable.ic_dialog_info)
                    .into(avatarImage)
            } ?: avatarImage.setImageResource(android.R.drawable.ic_dialog_info)
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Profile>() {

            override fun areItemsTheSame(oldItem: Profile, newItem: Profile): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Profile, newItem: Profile): Boolean =
                oldItem == newItem
        }
    }
}