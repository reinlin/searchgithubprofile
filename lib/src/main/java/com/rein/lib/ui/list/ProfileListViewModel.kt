package com.rein.lib.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import com.rein.lib.domain.ProfileRepository
import com.rein.lib.ui.BaseViewModel
import com.rein.lib.util.PAGE_SIZE
import javax.inject.Inject

class ProfileListViewModel @Inject constructor(
    private val repository: ProfileRepository
): BaseViewModel() {

    private val searchKeyword = MutableLiveData<String>()
    private val networkResult = map(searchKeyword) { keyword ->
        repository.search(keyword, PAGE_SIZE) { error ->
           message.value = error
        }
    }

    val message = MutableLiveData<String>()
    val profileList  = switchMap(networkResult) {
        it.profileList
    }!!
    val networkState = switchMap(networkResult) {
        it.networkState
    }!!

    fun updateKeyword(keyword: String): Boolean {
        if (searchKeyword.value == keyword) return false
        searchKeyword.value = keyword
        return true
    }

    fun refresh() {
        networkResult.value?.refresh?.invoke()
    }

    override fun onCleared() {
        super.onCleared()
        repository.unregister()
    }
}